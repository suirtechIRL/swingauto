package com.suirtech.swingauto.test.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"com.suirtech.swingauto.test.step_definitions"},
        features = "src/test/resources/features/Enter.feature",
        plugin = "json:target/HomePage-report.json"
)
public class HomePage2Runner {
}
