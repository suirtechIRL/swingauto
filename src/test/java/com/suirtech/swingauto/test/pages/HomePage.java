package com.suirtech.swingauto.test.pages;

import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.fixture.JButtonFixture;
import org.assertj.swing.fixture.JTextComponentFixture;

public class HomePage {

    private FrameFixture window;

    public HomePage(FrameFixture window) {
        this.window = window;
    }

    public JButtonFixture getEnterBtn() {
        return window.button("enterBtn");
    }

    public JTextComponentFixture getUserNameTF() {
        return window.textBox("userNameTF");
    }

    public JTextComponentFixture getF2() {
        return window.textBox("f2");
    }

}
