package com.suirtech.swingauto.test.step_definitions;

import com.suirtech.swingauto.Runner;
import com.suirtech.swingauto.test.pages.HomePage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.fixture.JButtonFixture;
import org.assertj.swing.fixture.JTextComponentFixture;
import org.assertj.swing.junit.testcase.AssertJSwingJUnitTestCase;

import javax.swing.*;

public class HomePageSteps {
    private FrameFixture window;
    private HomePage homePage;


    @Before
    public void beforeScenario() {
        FailOnThreadViolationRepaintManager.install();
        JFrame frame;
        frame = GuiActionRunner.execute(() -> Runner.start());
        window = new FrameFixture(frame);
        homePage = new HomePage(window);
    }

    @Given("^I open My App HomePage$")
    public void iOpenMyAppHomePage() throws Throwable {
        window.show();
    }

    @When("^I complete HomePage details \"([^\"]*)\", \"([^\"]*)\"$")
    public void iCompleteHomePageDetails(String userName, String pasword) throws Throwable {
        homePage.getUserNameTF().setText(userName);
        homePage.getF2().setText(pasword);
    }

    @Then("^I verify Logged in with \"([^\"]*)\", \"([^\"]*)\"$")
    public void iVerifyLoggedInWith(String userName, String pasword) throws Throwable {
        Thread.sleep(4000);
        homePage.getUserNameTF().requireText(userName);
        homePage.getF2().requireText(pasword);
    }

    @After
    public void afterScenario() {
        window.cleanUp();
    }
}
