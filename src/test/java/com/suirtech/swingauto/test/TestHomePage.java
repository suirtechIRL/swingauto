//package com.suirtech.swingauto.test;
//
//import com.suirtech.swingauto.Runner;
//import org.assertj.swing.core.GenericTypeMatcher;
//import org.assertj.swing.core.MouseButton;
//import org.assertj.swing.edt.GuiActionRunner;
//import org.assertj.swing.fixture.FrameFixture;
//import org.assertj.swing.fixture.JButtonFixture;
//import org.assertj.swing.fixture.JTextComponentFixture;
//import org.assertj.swing.junit.testcase.AssertJSwingJUnitTestCase;
//import org.junit.Before;
//import org.junit.Test;
//
//import javax.swing.*;
//
//
//public class TestHomePage extends AssertJSwingJUnitTestCase {
//
//    private FrameFixture window;
//
//    @Before
//    public void onSetUp() {
//        JFrame frame = null;
//        frame = GuiActionRunner.execute(() -> Runner.start());
//        window = new FrameFixture(robot(), frame);
//        window.show();
//    }
//
//    @Test
//    public void testMainButton() throws InterruptedException {
//
//        Thread.sleep(1000);
//        JButtonFixture btn = window.button("enterBtn");
//        JTextComponentFixture userNameTF = window.textBox("userNameTF");
//        JTextComponentFixture f2 = window.textBox("f2");
//        f2.setText("im a field");
//
//        btn.click();
//
//        userNameTF.requireText("Hello there");
//        f2.requireText("im a field");
//
//    }
//
//
//}
