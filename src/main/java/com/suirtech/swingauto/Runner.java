package com.suirtech.swingauto;

import com.suirtech.swingauto.ui.HomePage;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class Runner extends JFrame {

    static JFrame mainFrame;

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        start();
    }

    public static JFrame getMainFrame() {
        return mainFrame;
    }

    public static JFrame start() throws InvocationTargetException, InterruptedException {
        mainFrame = new JFrame("Shopping");
        mainFrame.setContentPane(new HomePage().mainPanel);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setVisible(true);
//        mainFrame.setSize(600,600);
        mainFrame.setMenuBar(new MenuBar());
        return mainFrame;
    }
}